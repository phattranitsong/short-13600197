{{--{{news.blade.php}}--}}
@extends('layouts.app')
@section('content')

    <nav  class="navbar navbar-light bg-info" style="font-family: 'Mitr';">
        <a class="navbar-brand" href="/new" style="color: #ffffff;">Short URL</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/new" style="color: #ffffff;" >Create Short URL  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/" style="color: #ffffff;" >List <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>

    <br>
    <h1 class="text-success" style="text-align: center; color: darkcyan;">Create Short URL</h1>
    <br>

<div class="border border-success" style="border-width:3px; ">
            <div class="container">
                <div class="row justify-content-md-center">

                    <div class="form-group col-9 p-2" style="text-align: center;">
                        <br>
                        <input type="text"  name="longurl" class="form-control" placeholder="Enter a URL here">
                    </div>

                    <div class="col-3 p-2" style="text-align: center;margin-top:20px;">
                        <button type="submit" class="btn btn-outline-info">CREATE SHORT URL</button>

                    </div>



                </div>

            </div>


{{--    <form method="post" action="{{ url('/')}}">--}}

{{--        <a href="{{url('/')}}" class="btn btn-danger" style="margin: 20px;">View all post</a>--}}
{{--    </form>--}}

</div>


@endsection
