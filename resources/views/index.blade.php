{{--{{index.blade.php}}--}}
@extends('layouts.app')
@section('content')


    <nav  class="navbar navbar-light bg-info" style="font-family: 'Mitr';">
        <a class="navbar-brand" href="/new" style="color: #ffffff;">Short URL</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/new" style="color: #ffffff;" >Create Short URL  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/" style="color: #ffffff;" >List <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <h1 class="text-center">All Short URL</h1>
    <br>
{{--    <a href="{{url('/new/')}}" class="btn btn-outline-success">Create New Short URL</a>--}}
{{--    <a href="{{url('/')}}" class="btn btn-danger">View all post</a>--}}



            <table class="table text-center">
                <thead class="thead" style="border-color: darkcyan; border-style: solid; border-width: 1px; border-radius: 10px;">
                <tr>
                    <th scope="col-2">Created-At</th>
                    <th scope="col">Long URL</th>
                    <th scope="col">Short URL</th>
                    <th scope="col">Views</th>
                    <th scope="col">Acion</th>

                </tr>
                </thead>

                @if(count($shorts) > 0)
                    @foreach($shorts as $short)
                <tbody>
                <tr>
                    <th>{{$short->created_at}}</th>
                    <td>{{$short->longurl}}</td>
                    <td> <input id="shorturl{{$short->id}}" class="form-control" type="text"
                                value="http://www.short.local/t/{{$short->shorturl}}" readonly></td>
                    <td>{{$short->view}}</td>
                    <td><button cl
{{--                    <td id="div1">{{'short.local/t/'.$short->shorturl}}</td>--}}
{{--                    <td>{{$short->view}}</td>--}}
{{--                    <td><button class="btn btn-outline-info" onclick="CopyToClipboard('div1')" type="button">Copy</button></td>--}}

                </tr>

                </tbody>

{{--            <div>--}}

{{--                <a href="{{url('/short/'.$short->id)}}"></a>--}}
{{--                <br>--}}
{{--                <h6>Created at :</h6>--}}
{{--                <p>{{$short->created_at}}</p>--}}
{{--                <h6>Long URL :</h6>--}}
{{--                <a href="{{url($short->longurl)}}">--}}
{{--                <p>{{$short->longurl}}</p>--}}
{{--                </a>--}}
{{--                <h6>Short URL :</h6>--}}
{{--                <div id="div1">--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('shorten.longurl',$short->shorturl) }}" target="_blank">{{ route('shorten.longurl',$short->shorturl) }}</a>--}}
{{--                    </td>--}}
{{--                    <p>{{'short.local/t/'.$short->shorturl}} &nbsp;--}}
{{--                        <button class="btn btn-secondary" onclick="CopyToClipboard('div1')" type="button">Copy</button>--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <h6>Views :</h6>--}}
{{--                <p>{{$short->view}}</p>--}}
{{--            </div>--}}
{{--            <hr>--}}
        @endforeach


    @endif



            </table>



    <script>
        function CopyToClipboard(containerid) {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(document.getElementById(containerid));
                range.select().createTextRange();
                document.execCommand("copy");

            } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(document.getElementById(containerid));
                window.getSelection().addRange(range);
                document.execCommand("copy");
                alert("text copied ")
            }}
    </script>



        <script>
        function copy(clickedBtn) {//clickedBtn คือ paramiter เฉยๆ
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shorturl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied '+ copyText.value);
        }

    </script>



@endsection

