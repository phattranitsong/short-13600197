<?php

namespace App\Http\Controllers;

use App\Short;
use Illuminate\Http\Request;

class ShortsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shorts = Short::OrderBy('created_at','desc')->get();
        return view('index')->with('shorts',$shorts);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shortenLink($shorturl)
    {
//        $short = Short::find($id);
        $find = Short::where('shorturl', $shorturl)->first();
        $longurl;
        if($find){
//            return redirect($find->longurl);
//            return Redirect::to($longurl);
                view +1;
              return redirect('/')->with('success',$longurl);

        }else{
            return "ไมพบรหัส SHORT URL";
        }

    }

//    /**
//     * Display a listing of the resource.
//     *
//    * @return \Illuminate\Http\Response
//    */
//
//    public function redirect()
//    {
//
//        $shorts = \Short::where('shorturl',$shorturl->$url)->first();
//        if($shorts){
//            return "ไมพบรหัส SHORT URL";
//        }else{
////            return \Redirect::to($shorts);
//            return redirect('/')->with('success','Success short! : '.'www.short.local/t/'.$url);
//
//        }
//
//    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $charEng = "abcdefghijklmnopgrstuvwxyz";
        $num09 = "0123456789";
        $url ="";
        for($i = 0 ; $i<5; $i++){
            $url .= $num09[rand(0,strlen($num09))-1];
        }
        $url .= $charEng[rand(0,strlen($charEng))-1];
        $this->validate($request,
            [
                'longurl' =>'required',
            ]
        );
        $short = new short();
        $short->longurl = $request->input('longurl');
        $short->shorturl = $url;
        $short->view = 0;
        $short->save();
        return redirect('/new')->with('success','Success short! : '.'www.short.local/'.$url);
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shortlink)
    {
        //
        $shorts = Short::all();
        if(count($shorts)>0){
            foreach ($shorts as $short){
                if ($short->shorturl == $shortlink){
                    $short->view += 1;
                    $short->save();
                    return view('redirect')->with('longurl',$short->longurl);
                }
            }
        }
        return view('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
