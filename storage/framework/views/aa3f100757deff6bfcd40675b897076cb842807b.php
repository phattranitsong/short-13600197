<?php $__env->startSection('content'); ?>


    <nav  class="navbar navbar-light bg-info" style="font-family: 'Mitr';">
        <a class="navbar-brand" href="/new" style="color: #ffffff;">Short URL</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/new" style="color: #ffffff;" >Create Short URL  <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/" style="color: #ffffff;" >List <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <h1 class="text-center">All Short URL</h1>
    <br>





            <table class="table text-center">
                <thead class="thead" style="border-color: darkcyan; border-style: solid; border-width: 1px; border-radius: 10px;">
                <tr>
                    <th scope="col-2">Created-At</th>
                    <th scope="col">Long URL</th>
                    <th scope="col">Short URL</th>
                    <th scope="col">Views</th>
                    <th scope="col">Acion</th>

                </tr>
                </thead>

                <?php if(count($shorts) > 0): ?>
                    <?php $__currentLoopData = $shorts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $short): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tbody>
                <tr>
                    <th><?php echo e($short->created_at); ?></th>
                    <td><?php echo e($short->longurl); ?></td>

                    <td> <input id="shorturl<?php echo e($short->id); ?>" class="form-control" type="text"
                                value="http://www.short.local/t/<?php echo e($short->shorturl); ?>" readonly></td>
                    <td><?php echo e($short->view); ?></td>
                    <td><button class="btn btn-outline-info" onclick="copy(this)" value="<?php echo e($short->id); ?>"type="button">Copy</button></td>

                </tr>

                </tbody>
























        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


    <?php endif; ?>



            </table>



    <script>
        function CopyToClipboard(containerid) {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(document.getElementById(containerid));
                range.select().createTextRange();
                document.execCommand("copy");

            } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(document.getElementById(containerid));
                window.getSelection().addRange(range);
                document.execCommand("copy");
                alert("text copied ")
            }}
    </script>



        <script>
        function copy(clickedBtn) {//clickedBtn คือ paramiter เฉยๆ
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shorturl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied '+ copyText.value);
        }

    </script>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/index.blade.php ENDPATH**/ ?>